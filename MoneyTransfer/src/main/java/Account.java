import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account implements Serializable {
    Integer balance;
    Integer id;
    public Lock lock = new ReentrantLock();

    Account(Integer balance, Integer id) {
        this.balance = balance;
        this.id = id;
    }

    public void withdraw(Integer amount) {
        balance -= amount;
    }

    public void deposit(Integer amount) {
        balance += amount;
    }

}
