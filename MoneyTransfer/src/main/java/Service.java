import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Service {
    Util util = new Util();
    ExecutorService service = Executors.newCachedThreadPool();

    public void serviceOperator() throws IOException, ClassNotFoundException {
        util.fileOperator();


        for (int i = 0; i < 10; i++) {
            service.submit(new Runnable() {
                public void run() {
                    while (Bank.value>0) {
                        int randomNum1 = 1 + (int) (Math.random() * ((30 - 1) + 1));
                        int randomNum3 = 1 + (int) (Math.random() * ((30 - 1) + 1));
                        int randomNum4 = 1 + (int) (Math.random() * ((30 - 1) + 1));
                        int randomNum2 = 1 + (int) (Math.random() * ((30 - 1) + 1));

                        new Operation().transfer(util.list.get(0), util.list.get(1), randomNum1);
                        new Operation().transfer(util.list.get(1), util.list.get(2), randomNum3);
                        new Operation().transfer(util.list.get(2), util.list.get(0), randomNum4);
                        new Operation().transfer(util.list.get(0), util.list.get(2), randomNum2);
                    }
                }
            });

        }
    }
}
