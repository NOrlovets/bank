import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

import java.util.ArrayList;
import java.util.List;


public class Util {
    List<Account> list = new ArrayList<Account>();
    Logger logger = LoggerFactory.getLogger(Util.class);
    public void fileOperator() throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        for (int i = 0; i < 3; i += 1) {
            int rN = 1 + (int) (Math.random() * ((190 - 50) + 1));
            oos.writeObject(new Account(rN, i));
        }
        oos.flush();
        oos.close();


        FileInputStream fis = new FileInputStream("temp.out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        for (int i = 0; i < 3; i += 1) {
            list.add((Account) oin.readObject());
            logger.info("balance=" + list.get(i).balance);
        }
    }

}
