import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class Operation {
    Logger logger = LoggerFactory.getLogger(Operation.class);
    public void transfer(Account from, Account to, Integer amount) {

        while (true) {
            if (from.balance - amount >= 0) {
                if (Bank.value > 0) {
                    if (from.lock.tryLock()) {
                        try {
                            if (to.lock.tryLock()) {
                                try {
                                    logger.info(Thread.currentThread().getName());
                                    from.withdraw(amount);
                                    to.deposit(amount);
                                    Bank.value--;
                                    break;
                                } finally {
                                    logger.info(from.id + " " + from.balance);
                                    logger.info(to.id + " " + to.balance);
                                    to.lock.unlock();
                                }
                            }
                        } finally {
                            from.lock.unlock();
                        }
                    }
                } else {
                    logger.info("END");
                    break;
                }
            } else {
                logger.info("Not enough money");
                //Thread.currentThread().yield();
                break;
            }
        }
    }
}
